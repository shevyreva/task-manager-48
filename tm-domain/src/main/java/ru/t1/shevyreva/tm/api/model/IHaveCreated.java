package ru.t1.shevyreva.tm.api.model;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHaveCreated {

    @NotNull
    Date getCreated();

    void setCreated(@NotNull Date created);

}
