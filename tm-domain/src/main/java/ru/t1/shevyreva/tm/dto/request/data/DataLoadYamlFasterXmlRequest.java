package ru.t1.shevyreva.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

public class DataLoadYamlFasterXmlRequest extends AbstractUserRequest {

    public DataLoadYamlFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
