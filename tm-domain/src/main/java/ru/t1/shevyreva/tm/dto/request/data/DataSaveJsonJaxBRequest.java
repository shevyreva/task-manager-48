package ru.t1.shevyreva.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

public class DataSaveJsonJaxBRequest extends AbstractUserRequest {

    public DataSaveJsonJaxBRequest(@Nullable String token) {
        super(token);
    }

}
