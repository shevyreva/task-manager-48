package ru.t1.shevyreva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "User profile change.";

    @NotNull
    private final String NAME = "user-profile-update";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("Enter Last Name:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter First Name:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter Middle Name:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken(), firstName, middleName, lastName);
        getUserEndpoint().updateProfile(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
