package ru.t1.shevyreva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.AbstractModel;

import java.util.List;

public interface IAbstractModelRepository<M extends AbstractModel> {
    void add(M model);

    void update(M model);

    void clear();

    @Nullable List<M> findAll();

    M findOneById(@NotNull String id);

    void removeOneById(@NotNull String id);

    int getSize();
}
