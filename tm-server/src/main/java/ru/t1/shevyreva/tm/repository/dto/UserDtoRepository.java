package ru.t1.shevyreva.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.shevyreva.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDtoRepository extends AbstractModelDtoRepository<UserDTO> implements IUserDtoRepository {

    public UserDtoRepository(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM UserDTO")
                .executeUpdate();
    }

    @Override
    public @Nullable List<UserDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM UserDTO e", UserDTO.class).getResultList();
    }

    @Override
    public @Nullable UserDTO findOneById(@NotNull String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Override
    public void removeOneById(@NotNull String id) {
        entityManager.createQuery("DELETE FROM UserDTO e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM UserDTO e", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Nullable
    public UserDTO findByLogin(@NotNull final String login) {
        return entityManager.createQuery("SELECT e FROM UserDTO e WHERE e.login =:login", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    public UserDTO findByEmail(@NotNull final String email) {
        return entityManager.createQuery("SELECT e FROM UserDTO e WHERE e.email = :email", UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeOne(@NotNull final UserDTO user) {
        entityManager.remove(user);
    }

}
